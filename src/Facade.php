<?php

namespace DaktaDeo\Silvasoft;

use Illuminate\Support\Facades\Facade as mother;

class Facade extends mother {
	protected static function getFacadeAccessor() {
		return 'Silvasoft';
	}
}