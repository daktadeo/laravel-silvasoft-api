<?php

namespace DaktaDeo\Silvasoft;
use DaktaDeo\Silvasoft\Exceptions\BadRequest;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

trait HasInvoices {
	/**
	 * From the Silvasoft docs:
	 *
	 *This GET method allows you to retrieve a list of invoices from your administration.
	 * You can also retrieve the details of a single invoice using the InvoiceNumber request parameter.
	 *
	 * Notes:
	 * Maximum 100 invoices will be returned per request
	 * You can retrieve more invoices by doing multiple requests and using the offset parameter
	 * The returned invoices will be sorted by invoice number (descending)
	 *
	 * @param array $parameters
	 *
	 * @return Collection of Invoices
	 * @throws Exception
	 */
	public function listSalesInvoices( Array $parameters ) {
		$arr = $this->getEndpointRequest( 'listsalesinvoices/', $parameters );
		$lst = Invoice::hydrate( $arr );
		
		return $lst->flatten();
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new sales invoice within the ‘Boekhouding’ (bookkeeping) module of our software.
	 *
	 * Notes:
	 *
	 * Maximum 50 invoice lines per invoice allowed.
	 * If CustomerNumber is used to identify the customer, the number must be greater then zero. A zero value will be interpreted as empty.
	 * 
	 * If CustomerName is used to identify the customer and you have multiple customers with exactly the same name, 
	 * we will use the first found match.
	 * 
	 * You must add at least one invoice line.
	 *
	 * @param Invoice $invoice
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addSalesTransaction( Invoice $invoice ) {
		if ( blank( $invoice->CustomerNumber )  && blank( $invoice->CustomerName )) {
				throw new IsRequired( "CustomerName or CustomerNumber" );
		}
		$options = $invoice->toArray();
		
		return $this->postEndpointRequest( 'addsalestransaction/', $options );
	}
	
	/**
	 * From the Silvasoft docs:
	 *
	 * This POST method allows you to create a new sales invoice and automatically mutate the invoiced products stock levels based on the invoice line quantities. This method will create an invoice within the ‘Facturatie’ (Invoicing) module of our software.
	 *
	 * Notes:
	 * Maximum 50 invoice lines per invoice allowed.
	 *
	 * If CustomerNumber is used to identify the customer, the number must be greater then zero.
	 *
	 * A zero value will be interpreted as empty.
	 *
	 * If CustomerName is used to identify the customer and you have multiple customers with exactly
	 * the same name, we will use the first found match.
	 *
	 * You are required to add at least one invoice line. The invoice line must either contain
	 * a parameter ‘ProductNumber’ or ‘ProductEAN’. The value entered in either of those fields must refer
	 * to an existing product in your Silvasoft administration.
	 *
	 * A stock mutation for the selected product will be automatically created.
	 *
	 * @param Invoice $invoice
	 *
	 * @return array
	 * @throws IsRequired
	 */
	public function addSalesInvoice( Invoice $invoice ) {
		if ( blank( $invoice->CustomerNumber )  && blank( $invoice->CustomerName )) {
			throw new IsRequired( "CustomerName or CustomerNumber" );
		}
		$options = $invoice->toArray();
		
		return $this->postEndpointRequest( 'addsalesinvoice/', $options );
	}
}