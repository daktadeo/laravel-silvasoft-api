<?php

namespace DaktaDeo\Silvasoft;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use hamburgscleanest\GuzzleAdvancedThrottle\Middleware\ThrottleMiddleware;

class Client {
	use HasEndPoints;
	use HasCRM;
	use HasProducts;
	use HasOrders;
	use HasInvoices;
	use NeedsThrottling;
	
	/** @var string */
	protected $apikey;
	/** @var string */
	protected $username;
	/** @var \GuzzleHttp\Client */
	protected $client;
	
	/**
	 * @param string $apikey
	 * @param string $username
	 * @param GuzzleClient|null $client
	 * @param int $api_request_limit
	 */
	public function __construct( string $apikey, string $username, GuzzleClient $client = null, int $api_request_limit = 15 ) {
		$stack = new HandlerStack();
		$stack->setHandler( new CurlHandler() );
		
		$throttle = new ThrottleMiddleware(
			$this->getRequestLimitRuleset( $api_request_limit )
		);
		$stack->push( $throttle->handle() );
		
		$this->apikey   = $apikey;
		$this->username = $username;
		$this->client   = $client ?? new GuzzleClient( [
				'headers'  => [
					'Content-Type' => "application/json",
					'ApiKey'       => "{$this->apikey}",
					'Username'     => "{$this->username}",
				],
				'base_uri' => 'https://mijn.silvasoft.nl',
				'handler'  => $stack
			] );
	}
	
	/**
	 * Multiplies the two given numbers. This has nothing to do with Silvasoft, but it allowed me to write a quick test
	 * to ensure my Facade, Service provider is working
	 *
	 * @param int $a
	 * @param int $b
	 *
	 * @return int
	 */
	public function multiply( $a, $b ) {
		return $a * $b;
	}
}