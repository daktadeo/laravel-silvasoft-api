<?php

namespace DaktaDeo\Silvasoft;

use Illuminate\Support\ServiceProvider as mother;

class ServiceProvider extends mother {
	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__.'/config.php' => config_path('silvasoft.php'),
		]);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		$this->registerSilvasoft();
	}
	/**
	 * {@inheritDoc}
	 */
	public function provides()
	{
		return [
			'silvasoft'
		];
	}
	/**
	 * Register the Silvasoft API class.
	 *
	 * @return void
	 */
	protected function registerSilvasoft()
	{
		$this->app->singleton(Client::class, function ($app) {
			$config = $app['config']->get('silvasoft');
			$cache_config = $app['config']->get('cache');
			
			$api_key = isset($config['api_key']) ? $config['api_key'] : "b7d343Bnhd436f3ec3bd3504582";
			$username = isset($config['username']) ? $config['username'] : "john@doe.nl";
			$api_request_limit = isset($config['api_request_limit']) ? $config['api_request_limit'] : 15;
			
			return new Client($api_key, $username,null,$api_request_limit,"laravel",$cache_config);
		});
		$this->app->alias(Client::class, 'Silvasoft');
	}

}