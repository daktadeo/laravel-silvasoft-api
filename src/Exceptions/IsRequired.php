<?php

namespace DaktaDeo\Silvasoft\Exceptions;
use Exception;

class IsRequired extends Exception
{
	/**
	 * an required attribute isn't provided
	 *
	 * @var string|null
	 */
	public function __construct(string $field)
	{
		parent::__construct($field." is required");
	}
}