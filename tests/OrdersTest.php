<?php
namespace DaktaDeo\Silvasoft\Test;

use DaktaDeo\Silvasoft\Client;
use DaktaDeo\Silvasoft\Exceptions\IsRequired;
use DaktaDeo\Silvasoft\Order;
use GuzzleHttp\Client as GuzzleClient;
//use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Silvasoft;

class OrdersTest extends TestCase {
	use HasMockGuzzleRequests;
	
	protected $test_key = "b7d343Bnhd436f3ec3bd3504582";
	protected $test_username = "john@doe.nl";
	
	/** @test */
	public function it_can_get_all_orders() {
		$response   = array (
			0 =>
				array (
					'CustomerNumber' => 123456,
					'OrderNotes' => '
Order from System X
n',
					'OrderReference' => 'system-X-1234',
					'OrderNumber' => 43,
					'OrderDiscountPc' => 15,
					'CustomerName' => 'Dhr. John sdf Doe dd',
					'Order_Orderline' =>
						array (
							0 =>
								array (
									'TaxPc' => 21,
									'Description' => 'Green t-shirt, size 6',
									'UnitPriceExclTax' => 3.95000000000000017763568394002504646778106689453125,
									'Quantity' => 36,
									'ProductNumber' => 'CD-X-2',
									'SubTotalInclTax' => 172.06000000000000227373675443232059478759765625,
									'ProductEAN' => NULL,
								),
							1 =>
								array (
									'TaxPc' => 6,
									'Description' => '',
									'UnitPriceExclTax' => 377.35849056603774442919529974460601806640625,
									'Quantity' => 1,
									'ProductNumber' => 'CD-X-18',
									'SubTotalInclTax' => 400,
									'ProductEAN' => NULL,
								),
						),
					'OrderTotalInclTax' => 486.25,
					'OrderDate' => '24-11-2016',
				),
			1 =>
				array (
					'CustomerNumber' => 24234337,
					'OrderNotes' => '
Order from System X
n',
					'OrderReference' => 'system-X-1234',
					'OrderNumber' => 44,
					'OrderDiscountPc' => 15,
					'CustomerName' => 'Gruis die via shop heeft besteld',
					'Order_Orderline' =>
						array (
							0 =>
								array (
									'TaxPc' => 21,
									'Description' => 'Green t-shirt, size 6',
									'UnitPriceExclTax' => 3.95000000000000017763568394002504646778106689453125,
									'Quantity' => 36,
									'ProductNumber' => 'CD-X-2',
									'SubTotalInclTax' => 172.06000000000000227373675443232059478759765625,
									'ProductEAN' => NULL,
								),
							1 =>
								array (
									'TaxPc' => 6,
									'Description' => '',
									'UnitPriceExclTax' => 377.35849056603774442919529974460601806640625,
									'Quantity' => 1,
									'ProductNumber' => 'CD-X-18',
									'SubTotalInclTax' => 400,
									'ProductEAN' => NULL,
								),
						),
					'OrderTotalInclTax' => 486.25,
					'OrderDate' => '23-11-2016',
				),
		);
		$endpoint   = 'https://mijn.silvasoft.nl/rest/listorders/';
		$parameters = [ "limit" => 100, "relationtype" => "All" ];
		
		$mockGuzzle = $this->mock_get_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$items      = $client->listOrders( $parameters );
		$this->assertCount( 2, $items );
	}
	

	
	/** @test */
	public function it_can_add_a_new_order(){
		$endpoint   = 'https://mijn.silvasoft.nl/rest/addorder/';
		$response   = [array (
			'CustomerNumber' => 123456,
			'OrderNotes' => 'Order from System X',
			'OrderReference' => 'system-X-1234',
			'Order_Orderline' =>
				array (
					0 =>
						array (
							'ProductNumber' => 'CD-X-2',
							'Quantity' => '36',
							'TaxPC' => 21,
							'UnitPriceExclTax' => 3.95000000000000017763568394002504646778106689453125,
							'Description' => 'Green t-shirt, size 6',
						),
					1 =>
						array (
							'ProductNumber' => 'CD-X-18',
							'Quantity' => '1',
							'TaxPC' => 6,
							'SubTotalInclTax' => 400,
						),
				),
		)];
		
		$data = Order::hydrate($response);
		$data->flatten();
		$order = $data->first();
		$parameters = ["json" => $order->toArray()];
		
		$mockGuzzle = $this->mock_post_guzzle_request( json_encode( $response ), $endpoint, $parameters );
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		
		$items = $client->addOrder($order);
		
		$this->assertCount( 1, $items );
		$this->assertSame($order->toArray(), $items[0]);
	}
	
	/** @test */
	public function when_posting_a_new_order_fields_are_required(){
		$this->expectException(IsRequired::class);
		
		$order = new Order();
		$order->OrderNotes = "Order description";
		
		$mockGuzzle = $this->getMockBuilder( GuzzleClient::class )
		                   ->setMethods( [ "post" ] )
		                   ->getMock();
		
		$client     = new Client( $this->test_key, $this->test_username, $mockGuzzle );
		$client->addOrder($order);
	}
}