This is a Laravel/PHP implementation of the [Silvasoft API](http://developers.silvasoft.nl/). 
It contains all the methods it offered to date.

**NOTE: THEORETICAL AT THE MOMENT, I'M STILL IMPLEMENTING IT IN THE MAIN APP **

Here are a few examples on how you can use the package:

```php
$client = new DaktaDeo\SilvaSoft\Client($api_key,$username);

//list all relations
$client->listRelations([ "limit" => 100, "relationtype" => "All" ]);

```
###Laravel
put the key and username inside the .ENV file
```php
	/*
	 * The secret API key for Silvasoft
	 */
	'api_key' => env('SILVASOFT_API_KEY', "b7d343Bnhd436f3ec3bd3504582"),
	/*
	 * Your Silvasoft username
	 */
	'username' => env('SILVASOFT_USERNAME', "john@doe.nl"),
	
```
Use the Facade

```php
Silvasoft::listRelations([ "limit" => 100, "relationtype" => "All" ]);
```

## Installation

You can install the package via composer:

``` bash
composer require daktadeo/laravel-silvasoft-api
```

## Usage

The first thing you need to do is get an API key at Silvasoft. 
You can find this in your Silvasoft application, under "beheer > API". Caution: never share this key!

Silvasoft adds limits to the amount of requests you can do in an hour. The default is 15. You 
can upgrade this in the settings (but it's not free).


## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email veerle@daktadeo.be 
instead of using the issue tracker.

## Postcardware

You're free to use this package (it's [MIT-licensed](LICENSE.md)), but if it makes it to 
your production environment we highly appreciate you sending us a postcard from your hometown, 
mentioning which of our package(s) you are using.

Our address is: DaktaDeo, Heidestraat 104, 9050 Gentbrugge, Belgium.

We publish all received postcards [on our company website](https://daktadeo.be/opensource/postcards).

## Credits

- [Veerle Deschepper](https://github.com/gompje)
- All Contributors

## Inspired by

- [Freek Van der Herten](https://github.com/freekmurze) -- who showed me the way.

## Support us

DaktaDeo is a web agency based in Gent, Belgium. 
You'll find an overview of all our open source projects [on our website](https://daktadeo.be/opensource).

Does your business depend on our contributions? Reach out and support us on [Patreon](https://www.patreon.com/daktadeo). 
All pledges will be dedicated to allocating workforce on maintenance and new awesome stuff.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.